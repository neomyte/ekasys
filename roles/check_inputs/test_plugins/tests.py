from ansible.parsing.yaml.objects import AnsibleVaultEncryptedUnicode
from ansible.utils.unsafe_proxy import AnsibleUnsafeText
import validators
import re

PORT_MIN_VALUE = 0
PORT_MAX_VALUE = 65535

def is_port(v):
    '''Checks if the given value is a valid ports'''
    try:
        if(str(v)[0] == '0'): return False
    except: pass
    try:
        return PORT_MIN_VALUE <= int(v) <= PORT_MAX_VALUE
    except: return False

def is_param_list(v):
    '''Checks if the given value is a valid list of parameters'''
    def is_valid_param(param):
        return all([key in param for key in ['desc', 'name', 'type']])
    return type(v) == list and all([is_valid_param(param) for param in v])

class TestModule(object):
    '''Net tests expansion'''
    def tests(self):
        return {
            'secret': lambda v: type(v) in [AnsibleVaultEncryptedUnicode, AnsibleUnsafeText],
            'list':   lambda v: type(v) in [list],
            'port':   is_port,
            'list_of_param': is_param_list,
            'url':    validators.url,
            'email':  validators.email,
            'ipv4':   validators.ipv4,
            'ipv6':   validators.ipv6
        }