# Test folder

There two types of tests:
  - **local**: Runned with `./local_test`(within this folder). It will only test the role locally.
  - **release**: Runned with `./release_test` (within this folder). It will try install the role from its remote source.

The `release` test will use the file `requirements.yml`. In this file no version were specified, thus the master branch is used. Fell free to specify another version if needed to.