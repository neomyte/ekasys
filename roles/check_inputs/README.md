# stra_check_inputs

This is role is supposed to be used in the meta of any other role. Its main purpose is to ensure that every parameters the role needs are defined and have the correct type.

## Requirements

Ansible version should be abose `2.8.5`.  
Import the `validators` package.

## Role Variables

This role is going to expect a parameter called `expected_params`.  
It is expected to be a list of parameter. Each parameter is defined a map containing the following keys:
 - **name**: The name of the parameter
 - **type**: The type of the parameter
 - **desc**: A short description of what the value this parameter is supposed to hold.  

The `type` value can be any of:
  - **[The jinja builtin tests](https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-tests)**
  - **[The ansible core tests](https://github.com/ansible/ansible/tree/devel/lib/ansible/plugins/test)**
  - **secret**: A vaulted value
  - **port**: A valid port
  - **ipv4**: An IPv4
  - **ipv6**: An IPv6
  - **url**: A URL
  - **email**: An email address
  - **list**: A list


## Dependencies

None.

## Example

This role is intended to be used in the `meta` of role that way:
```yaml
dependencies:
  - role: stra_check_inputs
    src:  https://gitlab.com/inastra/stra_check_inputs.git
    scm:  git
    vars:
      expected_params:
        - name: a_param
          type: string
          desc: The greatest of all parameter
        - name: another_param
          type: port
          desc: An even better parameter
```

## License

GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

## Author Information

**A nice quote**: *mota'séjya sa ho sa nass'koréa 'no léhinéwinsarya*
