# Eka System

To run:
> ansLts
> saplay eka.yml -i inventories/eka.yml

Gitlab currently unused

# TODO


 - [ ] Install helm for all users (+autocompletion)
 - [ ] Add `minikube deploy-env` res to runner profile
 - [ ] Add .minikube/certs to runner .conf/certs
 - [ ] Add certs needed from .kube/conf to .conf
 - [ ] Add .kube/conf to runner
 - [ ] Install and configure keycloack (cf ekadeps + psql --username admin --dbname overdb_db)
 - [ ] See below

```bash
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

# stop socat80 and 443
# sudo certbot certonly --standalone
# restart socats
```
 - [ ] Install created certs (kubectl edit secret -n ingress... sable...) privkey -> key, fullchain -> crt, base64, /etc/letsencrypt/live/neozigg.com/fullchain.pem
 - [ ] On keycloak, increase SSO session lifepan and client as well
