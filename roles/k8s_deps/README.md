# k8s_deps

This role handles k8s dependencies for roles using the k8s cluster.

## Requirements

N/A

## Role Variables

Expects an `app_name` variable of type string.

## Dependencies

N/A

## Example

This role is intended to be used in the `meta` of another role that way:
```yaml
dependencies:
  - role: k8s_deps
    vars:
      app_name: name_of_the_app
      # optionnal
      additional_envs:
        - staging
        - preprod
```

## License

GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

## Author Information

Neomyte aka Chaest
